(function() {
  'use strict';

  var cacheName = 'AGFolioPWA-v-2-6';
  var filesToCache = [
    './index.html',
    './pages/home.html',
    './pages/skills.html',
    './pages/work.html',
    './pages/contact.html',
    './scripts/app.js',
    './styles/inline.css',
    './images/ic_add_white_24px.svg',
    './images/ic_close_black_48px.svg',
    './images/ic_menu_white_48px.svg',
    './images/ic_refresh_white_24px.svg'
  ];

  self.addEventListener('install', function(e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(
      caches.open(cacheName).then(function(cache) {
        console.log('[ServiceWorker] Caching app shell');
        return cache.addAll(filesToCache);
        })
      );
  });

  self.addEventListener('activate', function(e) {
    console.log('[ServiceWorker] Activate');
    e.waitUntil(
      caches.keys().then(function(keyList) {
        return Promise.all(keyList.map(function(key) {
          if (key !== cacheName) {
            console.log('[ServiceWorker] Removing old cache', key, "to", cacheName);
          return caches.delete(key);
          }
        }));
      })
    );
    return self.clients.claim();
  });

self.addEventListener('fetch', function(e) {
  console.log('[Service Worker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      console.log(response);
      return response || fetch(e.request);
    })
  );
});

})();
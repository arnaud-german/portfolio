(function() {
  'use strict';

  var app = {
    isLoading: true,
    showMenu: false,
    spinner: document.querySelector('.loader'),
    container: document.querySelector('.main'),
    currenPage:"",
    pages: {
      home: {
        title: "Acceuil",
        url: "./pages/home.html"
      },
      skills: {
        title: "Compétences",
        url: "./pages/skills.html"
      },
      work: {
        title: "Travaux",
        url: "./pages/work.html"
      },
      contact: {
        title: "Contact",
        url: "./pages/contact.html"
      }
    }
  };


  /*****************************************************************************
  *
  * Event listeners for UI elements
  *
  *****************************************************************************/

  document.getElementById('menuBtn').addEventListener('click', function() {
    toggleMenu();  
  });


  /*****************************************************************************
  *
  * Methods to update/refresh the UI
  *
  *****************************************************************************/

  function toggleMenu(){
    if (!app.showMenu)
    {
      app.showMenu = true;
      app.container.innerHTML = "";
      for(var page in app.pages) {
        var currenPage = ""+page;
        var menuItem = document.createElement("div");
        menuItem.classList.add("menuItem");
        menuItem.innerHTML = app.pages[page].title;
        menuItem.id = page;
        menuItem.addEventListener('click', function(item) {
          app.showMenu = false;
          loadContent(item.srcElement.id);
        });
        app.container.append(menuItem);
      }
      var closeBtn = document.createElement("div");
      closeBtn.innerHTML = "&nbsp;";
      closeBtn.classList.add("menuItem");
      closeBtn.classList.add("closeBtn");
      closeBtn.addEventListener('click', function(){
        toggleMenu();
      });
      app.container.append(closeBtn);
    }
    else {
      app.showMenu = false;
      loadContent(app.currenPage);
    }
  }

  /*
  *  load the content in main div
  */
  function loadContent(pageName) {
    toggleSpinner(true);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function (e) { 
      if (xhr.readyState == 4 && xhr.status == 200) {
        app.container.innerHTML = xhr.responseText;
        app.currenPage = pageName;
        toggleSpinner(false);
      }
    }
    xhr.open("GET", app.pages[pageName].url, true);
    xhr.setRequestHeader('Content-type', 'text/html');
    xhr.send();
  }

  /*
  *  toggle the spinner
  */
  function toggleSpinner(switchOn) {
    if (switchOn != "undefined")
    {
      if (switchOn) {
        if (app.isLoading)
        {
          app.spinner.classList.add("hide");
          app.isLoading = false;
        }
      }
      else {
        if (app.isLoading) {
          app.spinner.classList.add("hide");
          app.isLoading = false;
        }    
      }
    }
    else {
      if (app.isLoading)
      {
        app.spinner.classList.add("hide");
        app.isLoading = false;
      }
      else 
      {
        console.log("start loading");
        app.spinner.classList.remove("hide");
        app.isLoading = true;
      }  
    }
  }

  /*****************************************************************************
  *
  * Methods for dealing with the model
  *
  *****************************************************************************/

  



  /*****************************************************************************
  *
  * Startup code
  *
  *****************************************************************************/

  loadContent("home");
  


  
  /*****************************************************************************
  *
  * Service worker
  *
  *****************************************************************************/

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register('./service-worker.js')
             .then(function() { console.log('Service Worker Registered'); });
  }


})();
